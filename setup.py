from setuptools import setup

setup(
    name='moretyme',
    version='0.0',
    description='Functions created to work with data pertaining to the MoreTyme loan product',
    url='https://bitbucket.org/dustin_yang/moretyme',
    author='Dustin Yang',
    author_email='dustin.yang@tyme.com',
    packages=[
    'moretyme', 
    'moretyme.activation', 
    'moretyme.scoring_table', 
    'moretyme.eligibility',
    'moretyme.application', 
    'moretyme.map'],
    install_requires=['pyspark>=0.0', 'pyspark_helper>=0.0'],
    zip_safe=False
)