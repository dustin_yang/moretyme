# Imports from other custom packages
from pyspark_helper.io import read_snowflake

# Typing imports
from typing import Optional, Dict
from pyspark.sql import DataFrame as SparkDataFrame
from datetime import date

# Imports from other commonly used packages
import datetime as dt
import pyspark.sql.functions as f
from pyspark.sql.window import Window

def get_activation(
        globals_: Dict,
        end_date: date=dt.date.today()
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        The date after the last permitted ACTIVATION_DATE (i.e. ACTIVATION_DATE < end_date)
        AND
        the last date allowed for EDW_TIMESTAMP (i.e. f.to_date(EDW_TIMESTAMP) <= end_date). 
        The default is dt.date.today().

    Returns
    -------
    SparkDataFrame
        Raw MORETYME.PUBLIC.MT_ACTIVATION data with column names changed and column types corrected.
    '''
    activation = (
        read_snowflake(globals_, 'MORETYME.PUBLIC.MT_ACTIVATION')
        .select(
            f.col('ID').alias('ACTIVATION_ID').cast('int'),
            f.col('PROFILE_ID').alias('ACCOUNTHOLDERKEY'),
            'SA_ID',
            f.col('PROFILE_TYPE').alias('ACTIVATION_PROFILE_TYPE'),
            f.col('ACCOUNT_NUMBER').alias('MORETYME_ACCOUNT_NUMBER'),
            f.col('INTERNAL_APPROVE').alias('ACTIVATION_INTERNAL_APPROVAL_INDICATOR').cast('string').cast('boolean'),
            f.col('EXTERNAL_APPROVE').alias('ACTIVATION_EXTERNAL_APPROVAL_INDICATOR').cast('string').cast('boolean'),
            f.col('ACCEPTED_CONSENT').alias('ACTIVATION_CONSENT_APPROVAL_INDICATOR').cast('string').cast('boolean'),
            f.col('STATUS').alias('ACTIVATION_STATUS'),
            f.trunc('LAST_UPDATED_DATE', 'month').alias('ACTIVATION_MONTH'),
            f.to_date('LAST_UPDATED_DATE').alias('ACTIVATION_DATE'),
            f.col('LAST_UPDATED_DATE').alias('ACTIVATION_TIMESTAMP'),            
            f.col('ACTIVATE_CHANNEL').alias('ACTIVATION_CHANNEL'),
            f.col('IS_FRAUD').alias('ACTIVATION_FRAUD_INDICATOR'),
            'EDW_TIMESTAMP',
            'EDW_UPDATE_TIMESTAMP'
        )
        .where(
            (f.col('ACTIVATION_DATE') < end_date)
            & (f.to_date('EDW_TIMESTAMP') <= end_date)
            & (f.col('ACTIVATION_STATUS') != 'INTERNAL_QUALIFIED')
        )
    )
    
    return activation

def get_latest_activation(
        globals_: Optional[Dict],  
        end_date: Optional[date]=dt.date.today(),
        activation: Optional[SparkDataFrame]=None                                          
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    globals_ : Optional[Dict]
        Access settings for Snowflake & Microservice Database. 
        Must be supplied if activation is not supplied.
    end_date : Optional[date], optional
        Passed to get_activation() if activation is None.
         The default is dt.date.today().
    activation : Optional[SparkDataFrame], optional
        Activation data dervied from get_activation(). 
        The default is None.
    

    Returns
    -------
    SparkDataFrame
        Latest activation row for each ACTIVATION_ID before end_date.
    '''
    activation = get_activation(globals_, end_date) if activation is None else activation
    
    latest_activation = (
        activation        
        .withColumn('RN', 
                    f
                    .row_number()
                    .over(Window.partitionBy('ACTIVATION_ID').orderBy(f.desc('ACTIVATION_TIMESTAMP')))
                    )
        .where(        
            (f.col('RN') == 1)
        )
        .drop('RN')
    )
    
    return latest_activation