# Imports from other custom packages
from pyspark_helper.io import read_snowflake

# Typing imports
from typing import Optional, Dict
from pyspark.sql import DataFrame as SparkDataFrame

# Imports from other commonly used packages
import pyspark.sql.functions as f
from pyspark.sql.window import Window

'''
This method should ideally not be used to recover the declined reasons
because it only covers the top 3 reasons
'''
def get_declined_reason(
        globals_: Dict
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.

    Returns
    -------
    SparkDataFrame
        Raw MORETYME.PUBLIC.MT_DECLINED_REASON data with column names changed and column types corrected.
    '''
    declined_reason = (
        read_snowflake(globals_, 'MORETYME.PUBLIC.MT_DECLINED_REASON')
        .select(
            f.col('ID').alias('DECLINED_REASON_ID').cast('int'),
            f.col('CODE').alias('DECLINED_REASON_CODE'),
            f.col('CREDIT_INFO_ID').alias('CREDIT_INFORMATION_ID').cast('int'),
            f.col('MESSAGE').alias('DECLINED_REASON_MESSAGE'),
            f.to_date('LAST_UPDATED_DATE').alias('DECLINED_REASON_DATE'),
            f.col('LAST_UPDATED_DATE').alias('DECLINED_REASON_TIMESTAMP'),
            'EDW_TIMESTAMP',
            'EDW_UPDATE_TIMESTAMP'
        )
    )
    
    return declined_reason

def get_top_declined_reason(
        globals_: Dict,
        declined_reason: Optional[SparkDataFrame]=None
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    declined_reason : Optional[SparkDataFrame], optional
        Declined reason data derived from get_declined_reason(). 
        The default is None.

    Returns
    -------
    SparkDataFrame
        Subset to declined_reason with the final declined reason shown to the applicant for each CREDIT_INFORMATION_ID.
    '''
    declined_reason = get_declined_reason(globals_) if declined_reason is None else declined_reason
    
    top_declined_reason = (
        declined_reason
        .withColumn('RN', 
                    f
                    .row_number()
                    .over(
                        Window
                        .partitionBy('CREDIT_INFORMATION_ID')
                        .orderBy('DECLINED_REASON_ID')
                        )
                    )
        .where(
            f.col('RN') == 1
        )
        .drop('RN')
    )
    
    return top_declined_reason