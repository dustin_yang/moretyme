# Imports from other custom packages
from moretyme.activation import (
    get_activation, 
    get_latest_activation, 
    get_credit_information, 
    get_top_declined_reason
)

# Typing imports
from typing import Optional, Dict
from pyspark.sql import DataFrame as SparkDataFrame
from datetime import date

# Imports from other commonly used packages
import datetime as dt
import pyspark.sql.functions as f
from pyspark.sql.window import Window

def get_activation_details_(
        activation: SparkDataFrame,
        credit_information: SparkDataFrame,
        top_declined_reason: SparkDataFrame
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    activation : SparkDataFrame
        Activation data dervied from get_activation() or get_latest_activation(). 
        The default is None.
    credit_information : SparkDataFrame
        Credit information data dervied from get_credit_information(). 
        The default is None.
    top_declined_reason : SparkDataFrame
        Top declined reason data derived from get_top_declined_reason().
        The default is None.

    Returns
    -------
    SparkDataFrame
        Data containing all activations with their associated credit information and declined reason.
    '''    
    activation_details = (
        activation
        .join(credit_information, on=['ACTIVATION_ID'], how='left')
        .where(
            (f.col('CREDIT_INFORMATION_DATE') <= f.col('ACTIVATION_DATE'))
            | (f.col('CREDIT_INFORMATION_DATE').isNull())
        )
        # get the credit_information_id row relevant to each activation_id
        # there should only be 1 activation_id per activation_date
        .withColumn('RN', 
                    f
                    .row_number()
                    .over(
                        Window
                        .partitionBy('ACTIVATION_ID', 'ACTIVATION_DATE')
                        .orderBy(f.desc('CREDIT_INFORMATION_ID'))
                        )
                    )
        .where(
            (f.col('RN') == 1)
            | (f.col('CREDIT_INFORMATION_DATE').isNull())
        )
        .drop('RN')
        # get the declined reason (if any)
        .join(top_declined_reason, on=['CREDIT_INFORMATION_ID'], how='left')
        .select(
            'ACCOUNTHOLDERKEY', 'SA_ID', 'MORETYME_ACCOUNT_NUMBER',
            'ACTIVATION_ID', 'ACTIVATION_PROFILE_TYPE',
            'CREDIT_INFORMATION_ID', 'CREDIT_INFORMATION_REQUEST_ID',
            'DECLINED_REASON_ID',
            'ACTIVATION_STATUS', 'CREDIT_INFORMATION_SOURCE', 'CREDIT_INFORMATION_STATUS',
            'DECLINED_REASON_CODE', 'DECLINED_REASON_MESSAGE',
            'ACTIVATION_MONTH', 'ACTIVATION_DATE', 'CREDIT_INFORMATION_DATE', 'DECLINED_REASON_DATE',
            'ACTIVATION_TIMESTAMP', 'CREDIT_INFORMATION_TIMESTAMP', 'DECLINED_REASON_TIMESTAMP'
        )
    )
    
    return activation_details

def get_activation_details(
        globals_: Dict,
        end_date: date=dt.date.today(),
        activation: Optional[SparkDataFrame]=None,
        credit_information: Optional[SparkDataFrame]=None,
        top_declined_reason: Optional[SparkDataFrame]=None
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        Passed to get_activation() and/or get_credit_information() if activation/credit_information is None.
         The default is dt.date.today().
    activation : Optional[SparkDataFrame], optional
        Activation data dervied from get_activation(). 
        The default is None.
    credit_information : Optional[SparkDataFrame], optional
        Credit information data derived from get_credit_information(). 
        The default is None.
    top_declined_reason : Optional[SparkDataFrame], optional
        Top declined reason data derived from get_top_declined_reason(). 
        The default is None.

    Returns
    -------
    SparkDataFrame
        Data containing all activations with accompanying credit information and top declined reason.

    '''
    activation = get_activation(globals_, end_date) if activation is None else activation
    credit_information = get_credit_information(globals_, end_date) if credit_information is None else credit_information
    top_declined_reason = get_top_declined_reason(globals_) if top_declined_reason is None else top_declined_reason
    
    activation_details = get_activation_details_(activation, credit_information, top_declined_reason)
    
    return activation_details

def get_latest_activation_details(
        globals_: Dict,
        end_date: date=dt.date.today(),
        latest_activation: Optional[SparkDataFrame]=None,
        credit_information: Optional[SparkDataFrame]=None,
        top_declined_reason: Optional[SparkDataFrame]=None
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        Passed to get_latest_activation() and/or get_credit_information if latest_activation/credit_information is None.
         The default is dt.date.today().
    latest_activation : Optional[SparkDataFrame], optional
        Latest activation data dervied from get_latest_activation(). 
        The default is None.
    credit_information : Optional[SparkDataFrame], optional
        Credit information data derived from get_credit_information(). 
        The default is None.
    top_declined_reason : Optional[SparkDataFrame], optional
        Top declined reason data derived from get_top_declined_reason(). 
        The default is None.

    Returns
    -------
    SparkDataFrame
        Data containing the latest activations with accompanying credit information and top declined reason.

    '''
    latest_activation = get_latest_activation(globals_, end_date) if latest_activation is None else latest_activation
    credit_information = get_credit_information(globals_, end_date) if credit_information is None else credit_information
    top_declined_reason = get_top_declined_reason(globals_) if top_declined_reason is None else top_declined_reason
    
    latest_activation_details = get_activation_details_(latest_activation, credit_information, top_declined_reason)
    
    return latest_activation_details