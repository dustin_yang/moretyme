# Imports from other custom packages
from pyspark_helper.io import read_snowflake

# Typing imports
from typing import Dict
from pyspark.sql import DataFrame as SparkDataFrame
from datetime import date

# Imports from other commonly used packages
import datetime as dt
import pyspark.sql.functions as f
from pyspark.sql.window import Window

def get_credit_information(
        globals_: Dict,
        end_date: date=dt.date.today()
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        The date after the last permitted CREDIT_INFORMATION_DATE (i.e. CREDIT_INFORMATION_DATE < end_date)
        AND
        the last date allowed for EDW_TIMESTAMP (i.e. f.to_date(EDW_TIMESTAMP) <= end_date). 
        The default is dt.date.today().
        
    Returns
    -------
    SparkDataFrame
        Raw MORETYME.PUBLIC.MT_CREDIT_INFORMATION data with column names changed and column types corrected.
    '''
    credit_information = (
        read_snowflake(globals_, 'MORETYME.PUBLIC.MT_CREDIT_INFORMATION')
        .withColumn('CREDIT_INFORMATION_DATE', f.to_date('LAST_UPDATED_DATE'))
        .where(
            (f.col('CREDIT_INFORMATION_DATE') < end_date)
            & (f.to_date('EDW_TIMESTAMP') <= end_date) 
        )
        # Retain only the last credit information row per activation_id & credit_information_date
        .withColumn('RN', 
                    f
                    .row_number()
                    .over(
                        Window
                        .partitionBy('ACTIVATION_ID', 'CREDIT_INFORMATION_DATE')
                        .orderBy(f.desc('ID'))
                    )
        )
        .where(
            (f.col('RN') == 1)            
        )
        .drop('RN')
        .select(
            f.col('ID').alias('CREDIT_INFORMATION_ID').cast('int'),
            f.col('REQUEST_ID').alias('CREDIT_INFORMATION_REQUEST_ID'),
            f.col('ACTIVATION_ID').cast('int'),
            f.col('SOURCE').alias('CREDIT_INFORMATION_SOURCE'),
            f.col('STATUS').alias('CREDIT_INFORMATION_STATUS'),
            f.col('LAST_UPDATED_DATE').alias('CREDIT_INFORMATION_TIMESTAMP'),
            'CREDIT_INFORMATION_DATE',
            'EDW_TIMESTAMP',
            'EDW_UPDATE_TIMESTAMP'
        )        
    )
    
    return credit_information