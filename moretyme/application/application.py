# Imports from other custom packages
from pyspark_helper.sdf_ops import sdf_union_all
from moretyme.activation import (
    get_activation_details,
    get_latest_activation_details
)
from moretyme.eligibility import get_eligibility
from moretyme.scoring_table import get_live_score, get_internal_score

# Typing imports
from typing import Optional, Dict
from pyspark.sql import DataFrame as SparkDataFrame
from datetime import date

# Imports from other commonly used packages
import datetime as dt
import pyspark.sql.functions as f
from pyspark.sql.window import Window

def get_live_application_details_(
        activation_details: SparkDataFrame,
        live_score: SparkDataFrame
    ) -> SparkDataFrame:
    
    live_application_details = (
        activation_details
        .where(
          (
              (f.col('CREDIT_INFORMATION_SOURCE') != 'INTERNAL') 
          )
          | 
          (f.col('ACTIVATION_PROFILE_TYPE') == 'GN6')
        )
        .join(live_score, on=['CREDIT_INFORMATION_REQUEST_ID'], how='left')
    )
    
    return live_application_details

def get_internal_application_details_(
        activation_details: SparkDataFrame,
        internal_score: SparkDataFrame,
        eligibility: SparkDataFrame        
    ) -> SparkDataFrame:
    
    internal_application = (
      activation_details
      .where(
        (f.col('CREDIT_INFORMATION_SOURCE') == 'INTERNAL')
      )
    )
    
    internal_application_eligibility = (
      internal_application
      .join(eligibility, on=['ACCOUNTHOLDERKEY'], how='left')
      .where(
        (f.col('ELIGIBILITY_TIMESTAMP') <= f.col('ACTIVATION_TIMESTAMP'))
      )
      .withColumn('RN', f.row_number().over(Window.partitionBy('ACTIVATION_ID', 'ACTIVATION_TIMESTAMP').orderBy(f.desc('ELIGIBILITY_TIMESTAMP'))))
      .where(
        (f.col('RN') == 1) 
      )
      .join(internal_score, on=['SA_ID', 'LIMITSTRATEGY'])
      .select('ACTIVATION_ID', 'ACTIVATION_TIMESTAMP', 'PD', 'VAP_GMIPVALUE')
    )
    
    internal_application_no_eligibility = (
      internal_application
      .join(internal_application_eligibility, on=['ACTIVATION_ID', 'ACTIVATION_TIMESTAMP'], how='left_anti')
      .join(internal_score, on=['SA_ID'], how='left')
      .where(
        f.col('EFFECTIVE_DATE') <= f.col('ACTIVATION_DATE')
      )
      .withColumn('RN', f.row_number().over(Window.partitionBy('ACTIVATION_ID', 'ACTIVATION_TIMESTAMP').orderBy(f.desc('EFFECTIVE_DATE'))))
      .where(
        f.col('RN') == 1
      )
      .select('ACTIVATION_ID', 'ACTIVATION_TIMESTAMP', 'PD', 'VAP_GMIPVALUE')
    )
    
    internal_application_no_date_match = (
      internal_application
      .join(internal_application_eligibility, on=['ACTIVATION_ID', 'ACTIVATION_TIMESTAMP'], how='left_anti')
      .join(internal_application_no_eligibility, on=['ACTIVATION_ID', 'ACTIVATION_TIMESTAMP'], how='left_anti')
      .join(internal_score, on=['SA_ID'], how='left')
      .withColumn('RN', f.row_number().over(Window.partitionBy('ACTIVATION_ID', 'ACTIVATION_TIMESTAMP').orderBy('EFFECTIVE_DATE')))
      .where(
        f.col('RN') == 1
      )
      .select('ACTIVATION_ID', 'ACTIVATION_TIMESTAMP', 'PD', 'VAP_GMIPVALUE')
    )
    
    internal_application_with_pd = sdf_union_all([internal_application_eligibility, internal_application_no_eligibility, internal_application_no_date_match])
    
    internal_application_details = (
      internal_application
      .join(internal_application_with_pd, on=['ACTIVATION_ID', 'ACTIVATION_TIMESTAMP'])
    )
    
    return internal_application_details

def get_application_details(
        globals_: Dict,
        end_date: date=dt.date.today(),
        activation_details: Optional[SparkDataFrame]=None,
        live_score: Optional[SparkDataFrame]=None,
        internal_score: Optional[SparkDataFrame]=None,
        eligibility: Optional[SparkDataFrame]=None
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        Passed to get_activation_details()/get_internal_score()/get_eligibility() if activation/internal_score/eligibility is None.
         The default is dt.date.today().
    activation_details : Optional[SparkDataFrame], optional
        Activation details derived from get_activation_details(). 
        The default is None.
    live_score : Optional[SparkDataFrame], optional
        Live score data derived from get_live_score(). 
        The default is None.
    internal_score : Optional[SparkDataFrame], optional
        Internal score data derived from get_internal_score(). 
        The default is None.
    eligibility : Optional[SparkDataFrame], optional
        Eligibility data derived from get_eligibility(). 
        The default is None.

    Returns
    -------
    SparkDataFrame
        Activation details with PD and VAP_GMIPVALUE at point of application.

    '''
    
    
    activation_details = get_activation_details(globals_, end_date) if activation_details is None else activation_details
    live_score = get_live_score(globals_) if live_score is None else live_score
    internal_score = get_internal_score(globals_, end_date) if internal_score is None else internal_score
    eligibility = get_eligibility(globals_, end_date) if eligibility is None else eligibility
    
    application_details = sdf_union_all([
        get_live_application_details_(activation_details, live_score),
        get_internal_application_details_(activation_details, internal_score, eligibility)
    ])
    
    return application_details

def get_latest_application_details(
        globals_: Dict,
        end_date: date=dt.date.today(),
        latest_activation_details: Optional[SparkDataFrame]=None,
        live_score: Optional[SparkDataFrame]=None,
        internal_score: Optional[SparkDataFrame]=None,
        eligibility: Optional[SparkDataFrame]=None
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        Passed to get_activation_details()/get_internal_score()/get_eligibility() if activation/internal_score/eligibility is None.
         The default is dt.date.today().
    latest_activation_details : Optional[SparkDataFrame], optional
        Latest activation details derived from get_latest_activation_details(). 
        The default is None.
    live_score : Optional[SparkDataFrame], optional
        Live score data derived from get_live_score(). 
        The default is None.
    internal_score : Optional[SparkDataFrame], optional
        Internal score data derived from get_internal_score(). 
        The default is None.
    eligibility : Optional[SparkDataFrame], optional
        Eligibility data derived from get_eligibility(). 
        The default is None.

    Returns
    -------
    SparkDataFrame
        Activation details with PD and VAP_GMIPVALUE at point of application.

    '''
    
    
    latest_activation_details = get_latest_activation_details(globals_, end_date) if latest_activation_details is None else latest_activation_details
    live_score = get_live_score(globals_) if live_score is None else live_score
    internal_score = get_internal_score(globals_, end_date) if internal_score is None else internal_score
    eligibility = get_eligibility(globals_, end_date) if eligibility is None else eligibility
    
    latest_application_details = sdf_union_all([
        get_live_application_details_(latest_activation_details, live_score),
        get_internal_application_details_(latest_activation_details, internal_score, eligibility)
    ])
    
    return latest_application_details