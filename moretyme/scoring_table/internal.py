from pyspark_helper.io import read_snowflake

from typing import Dict
from pyspark.sql import DataFrame as SparkDataFrame
from datetime import date

import datetime as dt
import pyspark.sql.functions as f

def get_internal_score(
        globals_: Dict,
        end_date: date=dt.date.today()
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        The date after the last permitted EFFECTIVE_DATE (i.e. EFFECTIVE_DATE < end_date).
        EFFECTIVE_DATE is the date of the first Monday after RUN_DATE.
        The default is dt.date.today().

    Returns
    -------
    SparkDataFrame
        Internal scoring data (currently only PD and VAP_GMIPVALUE).

    '''
    
    internal_score = (
      read_snowflake(globals_, 'MORETYME.ANALYTICS.BATCH_OFFER_HISTORY')
      .withColumn('EFFECTIVE_DATE', f.next_day('RUN_DATE', 'mon'))
      .where(
        (f.col('EFFECTIVE_DATE') < end_date)
      )
      .select('SA_ID', 'PD', 'VAP_GMIPVALUE', 'EFFECTIVE_DATE', 'LIMITSTRATEGY')
    )
    
    return internal_score