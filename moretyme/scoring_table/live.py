from pyspark_helper.io import read_snowflake

from typing import Dict
from pyspark.sql import DataFrame as SparkDataFrame

import pyspark.sql.functions as f

def get_live_score(
        globals_: Dict
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.

    Returns
    -------
    SparkDataFrame
        Live scoring data (currently only PD and VAP_GMIPVALUE).

    '''
    
    live_score = (
        read_snowflake(globals_, table_name='DATAMARTS.PUBLIC.JM_APPLICATION_FILEV3')
        .where(
            (f.col('RESPONSE_DECISIONENGINEFLOW') == 'DOUBLECASH')
        )
        .select(
            f.col('RESPONSE_LOANAPPLICATIONNUMBER').alias('CREDIT_INFORMATION_REQUEST_ID'), 
            f.col('RESPONSE_ATTRIBUTES_SCORINGATTRIBUTES_SCORE').alias('PD').cast('double'), 
            f.col('RESPONSE_GMIP_VALUE').alias('VAP_GMIPVALUE').cast('double')
        )
    )
    
    return live_score