# Imports from other custom packages
from pyspark_helper.io import read_snowflake

# Typing imports
from typing import Dict
from pyspark.sql import DataFrame as SparkDataFrame

# Imports from other commonly used packages
import pyspark.sql.functions as f

def get_customer_map(
        globals_: Dict
    ) -> SparkDataFrame:
    
    accountholderkey_said_map = (
        read_snowflake(globals_, '"DATAMARTS"."PUBLIC"."CombinedMambuClientInfo"')
        .select(
            f.col('ENCODEDKEY').alias('ACCOUNTHOLDERKEY'),
            f.col('IDENTITY_NUMBER_CLIENTS').alias('SA_ID')
        )
    )
    
    consumer_eda = (
        read_snowflake(globals_, 'MAMBU.MAMBU.SAVINGSACCOUNT')
        .select(
            'ACCOUNTHOLDERKEY',
            f.col('ENCODEDKEY').alias('EDAACCOUNTKEY'),
            f.col('ID').alias('EDA_ACCOUNT_NUMBER')
        )
    )
    
    moretyme_account_number = (
        read_snowflake(globals_, 'MORETYME.PUBLIC.MT_ACTIVATION')
        .where(
            f.col('STATUS') == 'ACCOUNT_CREATED'    
        )
        .select(
            f.col('PROFILE_ID').alias('ACCOUNTHOLDERKEY'), 
            f.col('ACCOUNT_NUMBER').alias('MORETYME_ACCOUNT_NUMBER')
        )
    )
    
    customer_map = (
        accountholderkey_said_map
        .join(consumer_eda, on=['ACCOUNTHOLDERKEY'], how='left')
        .join(moretyme_account_number, on=['ACCOUNTHOLDERKEY'], how='left')
    )
    
    return customer_map

def get_moretyme_loan_account_map(
        globals_: Dict
    ) -> SparkDataFrame:
    
    moretyme_loan_account_map = (
        read_snowflake(globals_, 'MAMBU.MAMBU.LOANACCOUNT')
        .where(
            (f.col('PRODUCTTYPEKEY') == '8a928e9375b4afb10175b6b7fc6333db')    
            & (f.col('ACCOUNTSTATE') != 'PENDING_APPROVAL')
        )
        .select(
            'ACCOUNTHOLDERKEY',
            f.col('ENCODEDKEY').alias('LOANACCOUNTKEY'),
            f.col('ID').alias('LOAN_ACCOUNT_NUMBER')
        )
    )
    
    return moretyme_loan_account_map