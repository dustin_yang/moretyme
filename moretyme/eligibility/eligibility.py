# Imports from other custom packages
from pyspark_helper.io import read_snowflake

# Typing imports
from typing import Optional, Dict
from pyspark.sql import DataFrame as SparkDataFrame
from datetime import date

# Imports from other commonly used packages
import datetime as dt
import pyspark.sql.functions as f
from pyspark.sql.window import Window

def get_eligibility(
        globals_: Dict,
        end_date: date=dt.date.today()
    ) -> SparkDataFrame:
    '''
    

    Parameters
    ----------
    globals_ : Dict
        Access settings for Snowflake & Microservice Database.
    end_date : date, optional
        The date after the last permitted ELIGIBILITY_DATE (i.e. ELIGIBILITY_DATE < end_date)
        AND
        the last date allowed for EDW_TIMESTAMP (i.e. f.to_date(EDW_TIMESTAMP) <= end_date). 
        The default is dt.date.today().

    Returns
    -------
    SparkDataFrame
        Raw MORETYME.PUBLIC.MT_ELIGIBILITY data with column names changed and column types corrected.

    '''
    eligibility = (
        read_snowflake(globals_, 'MORETYME.PUBLIC.MT_ELIGIBILITY')
        .select(
            f.col('PROFILEID').alias('ACCOUNTHOLDERKEY'),
            'FACILITY',
            f.col('SOURCE').alias('ELIGIBILITY_SOURCE'),
            f.col('STATUS').alias('ELIGIBILITY_STATUS'),
            f.col('REASON').alias('ELIGIBILITY_DECLINED_REASON'),
            'LIMITSTRATEGY',
            f.to_date(f.col('LASTUPDATED') - f.expr('INTERVAL 2 HOURS')).alias('ELIGIBILITY_DATE'),
            (f.col('LASTUPDATED') - f.expr('INTERVAL 2 HOURS')).alias('ELIGIBILITY_TIMESTAMP'),
            'EDW_TIMESTAMP',
            'EDW_UPDATE_TIMESTAMP'
        )
        .where(
            (f.col('ELIGIBILITY_DATE') < end_date)
            & (f.to_date('EDW_TIMESTAMP') <= end_date)    
        )    
    )
    
    return eligibility

def get_latest_eligibility(
        globals_: Optional[Dict],  
        end_date: Optional[date]=dt.date.today(),
        eligibility: Optional[SparkDataFrame]=None
    ) -> SparkDataFrame:
    '''
    Parameters
    ----------
    globals_ : Optional[Dict]
        Access settings for Snowflake & Microservice Database. 
        Must be supplied if eligibility is not supplied.
    end_date : Optional[date], optional
        Passed to get_eligibility() if eligibility is None.
         The default is dt.date.today().
    eligibility : Optional[SparkDataFrame], optional
        Eligibility data dervied from get_eligibility(). 
        The default is None.
    

    Returns
    -------
    SparkDataFrame
        Latest eligibility row for each ACCOUNTHOLDERKEY before end_date.
    '''
    eligibility = get_eligibility(globals_, end_date) if eligibility is None else eligibility
    
    latest_eligibility = (
        eligibility        
        .withColumn('RN', 
                    f
                    .row_number()
                    .over(Window.partitionBy('ACCOUNTHOLDERKEY').orderBy(f.desc('ELIGIBILITY_TIMESTAMP')))
                    )
        .where(        
            (f.col('RN') == 1)
        )
        .drop('RN')
    )
    
    return latest_eligibility